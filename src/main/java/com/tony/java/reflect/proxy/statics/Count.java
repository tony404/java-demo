package com.tony.java.reflect.proxy.statics;

/**
 * Created by wtz on 2015/9/1.
 * 定义一个账户接口
 */
public interface Count {
    // 查看账户方法
    public void queryCount();

    // 修改账户方法
    public void updateCount();
}
