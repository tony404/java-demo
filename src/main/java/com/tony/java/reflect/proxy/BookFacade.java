package com.tony.java.reflect.proxy;

/**
 * Created by wtz on 2015/9/1.
 */
public interface BookFacade {
    public void addBook();
}
