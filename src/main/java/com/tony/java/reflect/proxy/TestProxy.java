package com.tony.java.reflect.proxy;

/**
 * Created by wtz on 2015/9/1.
 * JDK的动态代理依靠接口实现
 */
public class TestProxy {

    public static void main(String[] args) {
        BookFacadeProxy proxy = new BookFacadeProxy();
        BookFacade bookProxy = (BookFacade) proxy.bind(new BookFacadeImpl());
        bookProxy.addBook();
    }

}