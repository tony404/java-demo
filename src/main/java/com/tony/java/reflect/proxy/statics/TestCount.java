package com.tony.java.reflect.proxy.statics;

/**
 * Created by wtz on 2015/9/1.
 * 测试Count类
 */
public class TestCount {
    public static void main(String[] args) {
        CountImpl countImpl = new CountImpl();
        CountProxy countProxy = new CountProxy(countImpl);
        countProxy.updateCount();
        countProxy.queryCount();

    }
}
