package com.tony.java.reflect.proxy.cglib;

/**
 * Created by wtz on 2015/9/1.
 * 这个是没有实现接口的实现类
 */
public class BookFacadeImpl1 {
    public void addBook() {
        System.out.println("增加图书的普通方法...");
    }
}
