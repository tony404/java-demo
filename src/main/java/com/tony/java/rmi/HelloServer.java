package com.tony.java.rmi;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * Created by wtz on 2015/8/31.
 */
public class HelloServer {
    public static void main(String args[]) {

        try {
            //
            IHello rhello = new HelloImpl();
            //
            LocateRegistry.createRegistry(8888);

            //
            //
            Naming.bind("rmi://localhost:8888/RHello", rhello);
//            Naming.bind("//localhost:8888/RHello",rhello);

            System.out.println(">>>>>INFO:");
        } catch (RemoteException e) {
            System.out.println("");
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.out.println("");
            e.printStackTrace();
        } catch (MalformedURLException e) {
            System.out.println("");
            e.printStackTrace();
        }
    }
}
