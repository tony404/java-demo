package com.tony.java.nio.net.xdevelop.nioserver.event;


import com.tony.java.nio.net.xdevelop.nioserver.Request;
import com.tony.java.nio.net.xdevelop.nioserver.Response;

/**
 * <p>Title: </p>
 * @author starboy
 */

public interface ServerListener {

   /**
    */
   public void onError(String error);

   /**
    */
   public void onAccept() throws Exception;

   /**
    *
    * @param request
    */
   public void onAccepted(Request request) throws Exception;

   /**

    * @param request
    */
   public void onRead(Request request) throws Exception;

   /**
    * ��������ͻ��˷������󴥷����¼�
    * @param request �ͻ�������
    */
   public void onWrite(Request request, Response response) throws Exception;

   /**
    * ���ͻ�����������������Ӻ󴥷����¼�
    * @param request �ͻ�������
    */
   public void onClosed(Request request) throws Exception;
}
