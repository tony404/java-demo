package com.tony.hessian.service;

/**
 * Created by wtz on 2015/9/5.
 */
public interface HelloService {
    public String helloWorld(String message);
}
