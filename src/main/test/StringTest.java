import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wangtianzhi on 2016/3/23.
 */
public class StringTest {

    @Test
    public void test(){
        String str = "abc";
        this.changeString(str);
        Assert.assertEquals("Abc",str);
    }

    public void changeString(String str){

        String result = str.replace('a','A');
        System.out.println("result:" + result);
    }
}
