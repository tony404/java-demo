package com.tony.hessian;

import com.tony.hessian.service.HelloService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.net.MalformedURLException;

/**
 * Created by wtz on 2015/9/5.
 */
public class HessianServiceTest {
    @Test
    public void testService() throws MalformedURLException {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "hessian-client.xml");
        HelloService hello = (HelloService) context.getBean("hessianClient");
        System.out.println(hello.helloWorld("hessian-spring-demo"));
    }
}
