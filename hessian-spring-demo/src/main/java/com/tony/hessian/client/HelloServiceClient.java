package com.tony.hessian.client;

import com.caucho.hessian.client.HessianProxyFactory;
import com.tony.hessian.service.HelloService;

import java.net.MalformedURLException;

/**
 * Created by wtz on 2015/9/5.
 * 在client中调用需要导入service端的jar包
 */
public class HelloServiceClient{
public static void main(String args[]) throws MalformedURLException {
        String url = "http://localhost:8080/hessian";
        System.out.println(url);

        HessianProxyFactory factory = new HessianProxyFactory();
        HelloService helloService = (HelloService) factory.create(HelloService.class, url);
        System.out.println(helloService.helloWorld("tony"));
}
        }
