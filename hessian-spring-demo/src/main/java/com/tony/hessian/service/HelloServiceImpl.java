package com.tony.hessian.service;

/**
 * Created by wtz on 2015/9/5.
 */
public class HelloServiceImpl implements HelloService{

    @Override
    public String helloWorld(String message) {
        return "hello " + message;
    }
}
